<?php

use Illuminate\Database\Seeder;
use App\Model\Plan;
//use Stripe;

class MoneySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $stripe = new Stripe('your-stripe-api-key', 'your-stripe-api-version');

        DB::table('plans')->truncate();        
        $plans = config('constants.plans');
        foreach($plans as $slug => $plan){
            //echo $slug;
            $plan['slug']=$slug;            
            Plan::create($plan);
        }
    }
}
