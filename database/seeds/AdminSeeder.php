<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        User::create([
        	'email'=>'admin@gmail.com',
        	'username' => 'admin',
        	'name' 	   => 'Administration',
        	'password' => bcrypt('12345678'),
        	'type' 	   => 'admin'
        ]);
    }
}
