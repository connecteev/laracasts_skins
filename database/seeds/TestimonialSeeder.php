<?php

use Illuminate\Database\Seeder;
use App\Model\Testimonial;

class TestimonialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

    	$faker = \Faker\Factory::create();

    	
        for($i=1; $i<=15; $i++){
        	$testi = new Testimonial; 
        	$testi->name = $faker->name;
        	$testi->image = $faker->imageUrl;
        	$testi->details = $faker->text;
        	$testi->profession = $faker->jobTitle; 
            $testi->url       = $faker->url;
        	$testi->save();
        }
    }
}
