<?php

use Illuminate\Database\Seeder;

class DiscussionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->call(ChannelSeeder::class);

        factory(App\User::class, 1000)->create();
        factory(App\Model\Forum::class, 300)->create();
        factory(App\Model\ForumAnswer::class,1000)->create([
            'channel_id' => 1,
        ]);
    }
}
