<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);


        \Artisan::call('migrate:fresh');

        $this->call(AdminSeeder::class);
        $this->call(DiscussionSeeder::class);
        $this->call(LessonSeeder::class);
        $this->call(TestimonialSeeder::class);
        $this->call(MoneySeeder::class);
    }
}
