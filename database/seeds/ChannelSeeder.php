<?php

use Illuminate\Database\Seeder;
use App\Model\Channel;
use Faker\Generator as Faker;

// to run this from the command line, run:
// php artisan db:seed --class=ChannelSeeder
class ChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = \Faker\Factory::create();

      DB::table('channels')->truncate();

      // use Faker to create random channel names
      factory(App\Model\Channel::class, 20)->create();

      //$channels = config('constants.channels');
      $channels = $this->channels;
      foreach ($channels as $channel){
        $channel['slug'] = str_slug($channel['title']);
        //echo $channel['slug'] . "\n";

        // can also use colorName or other options 
        // see https://github.com/fzaninotto/Faker#fakerprovidercolor
        $channel['color'] = $faker->safeColorName;

        Channel::create($channel);
      }
    }

    /**
     * The List of channels for the Discussion Forum.
        All
        Assistance
        General
        Guides
        Tips
        Business and Strategy
        Career
        Design and User Experience
          Usability
        Entrepreneurship
        General
        Leadership
        Leadership and Business
        Marketing
          User Acquisition
          Growth
        Product Management
        Productivity
        Sales
        Software
        Interviewing
        Metrics
        Product Vision, Strategy and Roadmap
        Technology
     */
    protected $channels = [
      [
        'title' => 'Assistance',
        'details' => "Get help with something",
        //'color' => 'Green',
      ],
      [
        'title' => 'General',
        'details' => "General Questions or Topics",
      ],
      [
        'title' => 'Guides',
        'details' => "Guides to help you",
      ],
      [
        'title' => 'Tips',
        'details' => "Tips you can use",
      ],
      [
        'title' => 'Business and Strategy',
        'details' => "Business and Strategy Topics",
      ],
      [
        'title' => 'Career',
        'details' => "Career Questions or Topics, including Job Search, Interviewing etc.",
      ],
      
    ];

}
