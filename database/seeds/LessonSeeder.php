<?php

use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        factory(\App\Model\Skill::class, 10)->create();
        factory(\App\Model\Series::class, 150)->create();
        factory(\App\Model\Lesson::class, 1000)->create();
        factory(\App\Model\SeriesGroup::class, 10)->create();
        factory(\App\Model\SeriesGroupItem::class, 40)->create();
        factory(\App\Model\Tag::class, 50)->create();
        factory(\App\Model\TagItem::class, 500)->create();
    }
}
