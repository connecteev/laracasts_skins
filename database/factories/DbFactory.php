<?php

use Faker\Generator as Faker;


// Tags
$factory->define(App\Model\Tag::class, function (Faker $faker) {
    return [
        //
        'title'  => $faker->jobTitle,
        'details'=> $faker->text,
    ];
});

//Channels
$factory->define(App\Model\Channel::class, function (Faker $faker){
	return [
            'title' => $faker->jobTitle,
            'details' => $faker->text(200),
            'color'   => $faker->randomElement(['red', 'green', 'blue', 'orange', 'pink']),
		];
});

// Forums
$factory->define(App\Model\Forum::class, function(Faker $faker){
    return [
        'title'     => $faker->sentence(),
        'details'   => $faker->text(500),
        'user_id'   => $faker->numberBetween(1,1000),
        'channel_id' => $faker->numberBetween(1, 20),
        'best_answer' => $faker->numberBetween(1, 1000),
    ];
});

// ForumAnswers
$factory->define(App\Model\ForumAnswer::class, function(Faker $faker){
     return [
        'user_id' => $faker->numberBetween(1, 1000),        
        'forum_id' => $faker->numberBetween(1, 300),
        'details' => $faker->text( $faker->randomElement([200,300,400]) ),
     ];
});

// TagItems
$factory->define(App\Model\TagItem::class, function(Faker $faker){
     return [
        'tag_id' => $faker->numberBetween(1, 50),
        'type_id' => $faker->numberBetween(1, 100),
        'type'=>$faker->randomElement(['Lesson', 'Skill', 'Series', 'Tag']),
     ];
});

// Lessons
$factory->define(App\Model\Lesson::class, function (Faker $faker) {
    return [
        'title'  => $faker->sentence(),
        'details'=> $faker->text(),
        'series_id' => $faker->randomElement([ null, $faker->numberBetween(1, 150) ] ),
        'free' 	    => $faker->numberBetween(0,1),
		'image'  => $faker->imageUrl(),
		'video'  => asset('/uploads/aloha.mp4'),
        'development' => $faker->randomElement(['0','1']),
		'duration' => $faker->numberBetween(1,3000),
        'skill_id' => $faker->numberBetween(1, 10) ,
        'level'    => $faker->randomElement(['Intermediate', 'Beginner', 'Advanced']),
        'type'   => $faker->randomElement(['lesson', 'episode']),
    ];
});

// Skills
$factory->define(App\Model\Skill::class, function (Faker $faker) {
    return [
        'title'  => $faker->sentence(2),
        'details'=> $faker->text(100),
        'image'  => $faker->imageUrl(),
        'free' => $faker->numberBetween(0,1),
        //slug gets auto-filled since the Skill model uses sluggable
		'color'  => $faker->randomElement(['red', 'green', 'blue', 'orange', 'pink']),
		'development' => $faker->randomElement(['0','1']),
    ];
});


// Series 
$factory->define(App\Model\Series::class, function (Faker $faker) {
    return [
        'title'  => $faker->sentence(),
        'details'=> $faker->text(500),
        'skill_id' => $faker->numberBetween(1, 10),
        'level'    => $faker->randomElement(['Intermediate', 'Beginner', 'Advanced']),
        'free' 	 => $faker->numberBetween(0,1),
		'image'  => $faker->imageUrl(),
        'development' => $faker->randomElement(['0','1']),
    ];
});

// SeriesGroups
$factory->define(App\Model\SeriesGroup::class, function (Faker $faker) {
    return [
        //
        'title'  => $faker->sentence(4),
        'details'=> $faker->text,
    ];
});

// SeriesGroupItems
$factory->define(App\Model\SeriesGroupItem::class, function (Faker $faker) {
    return [
        'series_group_id'  => $faker->numberBetween(1,10),
        'series_id'=> $faker->numberBetween(1,150),
    ];
});

// Plans
$factory->define(App\Model\Plan::class, function (Faker $faker) {
    return [
        'title'  => $faker->numberBetween(1,5).' '. 'months',
        'details'=> $faker->text,
        'duration' => $faker->numberBetween(1,5).' '. 'months',
        'amount' => $faker->numberBetween(5,50),
    ];
});
