<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('amount');
            $table->integer('duration')->string();
            $table->enum('period', ['day', 'month', 'year','forever'])->default('month');
            $table->text('details')->nullable();
            $table->string('slug')->nullable();
            $table->enum('type', ['normal', 'business'])->default('normal');
            $table->string('button')->nullable();
            $table->integer('number')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
