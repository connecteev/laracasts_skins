<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserTableAddUsername extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('website')->nullable();
            $table->string('twitter')->nullable();
            $table->string('github')->nullable();
            $table->string('job_title')->nullable();
            $table->string('location')->nullable();
            $table->string('flag')->nullable();
            $table->string('employment')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('available_for_hire')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('website');
            $table->dropColumn('twitter');
            $table->dropColumn('github');
            $table->dropColumn('job_title');
            $table->dropColumn('location');
            $table->dropColumn('flag');
            $table->dropColumn('employment');
            $table->dropColumn('available_for_hire');
            $table->dropColumn('image');
        });
    }
}
