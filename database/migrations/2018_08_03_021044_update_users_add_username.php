<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAddUsername extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('username')->nullable();
            $table->string('expiry_date')->nullable();
            $table->integer('plan_id')->nullable();
            $table->enum('type', ['admin','user'])->default('user');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('username');
            $table->dropColumn('expiry_date');
            $table->dropColumn('plan_id');
            $table->dropColumn('type');
            $table->dropColumn('deleted_at');
            $table->dropColumn('status');
        });
    }
}
