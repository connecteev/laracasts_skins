<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->integer('skill_id')->nullable();
            $table->enum('level', ['beginner', 'intermediate', 'advanced'])->default('beginner');
            $table->integer('series_id')->nullable();
            $table->string('duration')->nullable();
            $table->boolean('free')->default(0);
            $table->enum('development', [0,1])->default(1);
            $table->string('slug')->nullable();
            $table->text('details')->nullable();
            $table->string('video')->nullable();
            $table->string('image')->nullable();
            $table->string('size')->nullable();
            $table->integer('step')->nullable()->default(1);
            $table->integer('views')->nullable()->default(0);
            $table->enum('type', ['episode', 'lesson'])->default('episode');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}
