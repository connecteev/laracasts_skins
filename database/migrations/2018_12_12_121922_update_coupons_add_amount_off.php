<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCouponsAddAmountOff extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function (Blueprint $table) {
            //
            $table->integer('amount_off')->nullable();
            $table->integer('duration_in_months')->nullable();
            $table->integer('max_redemptions')->nullable();
            $table->integer('percent_off')->nullable();
            $table->integer('redeem_by')->nullable();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function (Blueprint $table) {
            //
            $table->dropColumn('amount_off');
            $table->dropColumn('duration_in_months');
            $table->dropColumn('max_redemptions');
            $table->dropColumn('percent_off');
            $table->dropColumn('redeem_by');
        });
    }
}
