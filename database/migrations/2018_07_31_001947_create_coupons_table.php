<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('plan_id')->nullable();
            $table->string('code');
            $table->string('expiry_date')->nullable();
            $table->string('details')->nullable();
            $table->integer('user_id')->nullable();
            $table->enum('status',[0,1])->default(0);
            $table->enum('duration',['once','forever','repeating'])->default('once');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
