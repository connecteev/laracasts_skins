import Vue from "vue";
import VueRouter from "vue-router";

import Home from "./pages/Home";
import Search from "./pages/Search";
import Podcast from "./pages/Podcast";

Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "Home",
            component: Home
        },
        {
            path: "/search",
            name: "Search",
            component: Search
        },
        {
            path: "/podcast",
            name: "Podcast",
            component: Podcast
        }
    ]
});
