<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Channel extends Model
{
    //

    protected $fillable = ['title', 'details', 'slug','color'];
	use Sluggable, SoftDeletes; 

	    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function forums(){
        return $this->hasMany(Forum::class);
    }
}
