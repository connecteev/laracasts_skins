<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends Model
{
    //

        //use sluggable; 
        use SoftDeletes;

	protected $fillable = ['title', 'amount','duration','details', 'button','type', 'slug', 'number' , 'period', 'desctription','cost', 'name', 'slug' ]; 


    public function subscribers(){
        return $this->hasMany(Subscription::class, 'stripe_plan', 'slug');
    }

        public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
