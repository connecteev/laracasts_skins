<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Testimonial extends Model
{
    //
    use SoftDeletes;

    protected $fillable = ['name', 'image', 'content', 'profession','url', 'details'];
}
