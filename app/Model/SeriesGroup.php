<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;


class SeriesGroup extends Model
{
    //

    protected $fillable = ['title','slug','details' ];

    use Sluggable, SoftDeletes;
    
             public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function series(){
    	return $this->hasMany(SeriesGroupItem::class);
    }
}
