<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TagItem extends Model
{
    //

    protected $fillable = ['tag_id', 'type_id', 'type']; 

    function tag(){
    	return $this->belongsTo(Tag::class);
    }
}
