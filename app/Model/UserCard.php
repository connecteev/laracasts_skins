<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    //
    protected $fillable = ['token', 'user_id'];
}
