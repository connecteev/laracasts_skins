<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    //
    // protected $fillable=['email'];

    public function user(){
    	return $this->belongsTo(\App\User::class);
    }
}
