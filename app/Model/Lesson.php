<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Elasticquent\ElasticquentTrait;

class Lesson extends Model
{
    //

    protected $fillable = ['title', 'series_id', 'details','level', 'skill_id', 'type', 'development', 'free','step'];

    use Sluggable, SoftDeletes, ElasticquentTrait;

        public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function series(){
    	return $this->belongsTo(\App\Model\Series::class);
    }

    function timing(){
        return ( floor(($this->duration %3600) / 60 )) .':' .  ($this->duration%60)  ;
    }

    function getTags(){
        return $this->hasMany(TagItem::class,'type_id')->where('type','Lesson');
    }

    function isFavorite($user_id=null){
        if(!$user_id){
             if(!Auth::check()){
                return false; 
            }

            $user_id = Auth::user()->id;
        }
        return Favorite::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Lesson'])->first();
    }

    function isInWatchlist($user_id=null){
        if(!$user_id){
             if(!Auth::check()){
                return false; 
            }

            $user_id = Auth::user()->id;
        }
        return SeriesWatchlist::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Lesson'])->first();
    }

    function isCompleted($user_id=null){
         if(!$user_id){
            if(!Auth::check()){
                return false; 
            }
            $user_id = Auth::user()->id;
        }
        return LessonCompleted::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Lesson'])->first();
    }

    function canDownload(){
        if($this->free){
            return true; 
        }
        else{
            if(Auth::check()){
                if(Auth::user()->expiry_date>date('Y-m-d')){
                    return true;
                }
                else return false; 
            }
            else{
                return false; 
            }
        }
    }

    function skill(){
        return $this->belongsTo(\App\Model\Skill::class);
    }

    public function tags(){
        return $this->hasMany(TagItem::class, 'type_id')->where('type', 'Lesson');
    }

    public function nextLesson(){
        return Lesson::where(['series_id'=>$this->series_id, 'step'=>($this->step+1)])->first();
    }


}
