<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContentSubscription extends Model
{
    //

     protected $fillable = ['type_id', 'type', 'user_id'];

     function series(){
     	return $this->belongsTo(Series::class, 'type_id');
     }

     function forum(){
     	return $this->belongsTo(Forum::class, 'type_id');
     }
}
