<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    //
    use SoftDeletes;

    protected $fillable=['title', 'details','duration', 'plan_id', 'expiry_date', 'code', 'duration_in_months', 'max_redemptions', 'percent_off', 'redeem_by', 'amount_off'];

    function plan(){
    	return $this->belongsTo(Plan::class);
    }
}
