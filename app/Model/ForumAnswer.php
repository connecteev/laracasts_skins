<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ForumAnswer extends Model
{
    //
    use SoftDeletes;

    protected $fillable = ['user_id', 'forum_id', 'details', 'channel_id'];

   function channel(){
   	return $this->belongsTo(\App\Model\Channel::class);
   }

   function user(){
   	return $this->belongsTo(\App\User::class);
   }

   function forum(){
   	return $this->belongsTo(Forum::class);
   }

   function likes(){
    return $this->hasMany(Like::class, 'type_id')->where(['type'=>'ForumAnswer']);
   }

   function likeUsers(){
      return \App\User::join('likes', 'likes.user_id', '=', 'users.id')
                      ->where(['likes.type'=>'ForumAnswer', 'likes.type_id'=>$this->id]);
   }
}

