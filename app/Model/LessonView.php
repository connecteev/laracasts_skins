<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LessonView extends Model
{
    //

    protected $fillable = ['type_id', 'type', 'user_id'];
}
