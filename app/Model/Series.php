<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Elasticquent\ElasticquentTrait;

class Series extends Model
{
    //
    use Sluggable, SoftDeletes, ElasticquentTrait;

    protected $fillable = ['title', 'slug', 'details', 'skill_id', 'level', 'development', 'step'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ]; 
    }

    protected $mappingProperties = array(
        'title' => array(
             'type' => 'string',
             'analyzer' => 'standard'
         )
     );
     

    function skill(){
    	return $this->belongsTo(\App\Model\Skill::class);
    }

    function lessons(){
    	return $this->hasMany(\App\Model\Lesson::class)->orderBy('step');
    }

    function isSubscribed($user_id=null){
        if(!$user_id){
            if(!Auth::check()){
                return false; 
            }

            $user_id = Auth::user()->id;
        }

        return ContentSubscription::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Series'])->first();
    }

    function timing(){
        $duration = $this->lessons->sum('duration');
        return ( floor(($duration %3600) / 60 )) .':' .  ($duration%60)  ;
    }


    function isInWatchlist($user_id=null){
        if(!$user_id){
             if(!Auth::check()){
                return false; 
            }

            $user_id = Auth::user()->id;
        }
        return SeriesWatchlist::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Series'])->first();
    }

    function getRelated(){
        $groups = SeriesGroupItem::where(['series_id'=>$this->id])->pluck('series_group_id');

        $series = Series::leftjoin('series_group_items', 'series_group_items.series_id' , '=', 'series.id')
                    ->where(function($query) use ($groups) {
                        $query
                            ->whereIn('series_group_items.series_group_id', $groups)
                            ->orWhere('series.skill_id', $this->skill_id);
                    })
                    ->select(['series.*'])
                    ->where('series.id', '!=', $this->id)->get();
        return $series;
    }

    public function tags(){
        return $this->hasMany(TagItem::class, 'type_id')->where('type', 'Series');
    }


    function isCompleted($user_id=null){
        if(!$user_id){
           if(!Auth::check()){
               return false; 
           }
           $user_id = Auth::user()->id;
       }
       return LessonCompleted::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Series'])->first();
   }
}

