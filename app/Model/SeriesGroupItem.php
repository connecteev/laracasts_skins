<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeriesGroupItem extends Model
{
    //
    use SoftDeletes;

    function series(){
    	return $this->belongsTo(Series::class);
    }
}
