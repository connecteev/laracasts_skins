<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Elasticquent\ElasticquentTrait;

class Forum extends Model
{
    //

    use sluggable, SoftDeletes, ElasticquentTrait;

	protected $fillable = ['title', 'details', 'channel_id', 'user_id']; 

        public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function user(){
    	return $this->belongsTo(\App\User::class);
    }

    function channel(){
    	return $this->belongsTo(Channel::class);
    }

    function replies(){
        return $this->hasMany(ForumAnswer::class);
    }

    function isFavorite($user_id=null){
        if(!$user_id){
             if(!\Auth::check()){
                return false; 
            }

            $user_id = Auth::user()->id;
        }
        return Favorite::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Forum'])->first();
    }

    function isSubscribed($user_id =null){
        if(!$user_id){
             if(!\Auth::check()){
                return false; 
            }

            $user_id = Auth::user()->id;
        }
        return ContentSubscription::where(['type_id'=>$this->id, 'user_id'=>$user_id, 'type'=>'Forum'])->first();
    }

    function bestAnswer(){
        return $this->belongsTo(ForumAnswer::class, 'best_answer');
    }
}
