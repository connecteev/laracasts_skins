<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SeriesWatchlist extends Model
{
    //
    use SoftDeletes;

     protected $fillable = ['type_id', 'type', 'user_id'];
}
