<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //
       protected $fillable = ['type_id', 'type', 'user_id', 'details'];

       function user(){
       		return $this->belongsTo(\App\User::class);
       }
}
