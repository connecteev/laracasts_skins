<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    //
    use Sluggable, SoftDeletes;

    protected $fillable = ['title', 'details', 'image', 'free', 'slug', 'color', 'development'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function series(){
        return $this->hasMany(Series::class)->orderBy('step');
    }

    function lessons(){
        return $this->hasManyThrough(Lesson::class, Series::class)->orderBy('id','desc')->where(['type'=>'episode']);
    }

    function extraLessons(){
        return $this->hasMany(Lesson::class)->whereNull('series_id');
    }

    public function tags(){
        return $this->hasMany(TagItem::class, 'type_id')->where('type', 'Skill');
    }
}
