<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tag extends Model
{
    //
    use Sluggable, SoftDeletes;

    protected $fillable = ['title', 'slug', 'details', 'color'];

    
                public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    function items(){
        return $this->hasMany(TagItem::class);
    }
}
