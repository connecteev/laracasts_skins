let mix = require('laravel-mix');

const tailwindcss = require('tailwindcss');

// Removes unused CSS
// According to Discord chat: Running Purge CSS as part of Post CSS is a ton faster than laravel-mix-purgecss
// But if that doesn't work use https://github.com/spatie/laravel-mix-purgecss
const purgecss = require('@fullhuman/postcss-purgecss')({
  // Specify the paths to all of the template files in your project 
  content: [
    './resources/views/*.php',
    './resources/views/**/*.php',
    './resources/js/components/*.vue',
    './resources/js/components/**/*.vue',

    './resources/js/test/**/*.vue',
    './resources/js/test/**/**/*.vue',

  ],

  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
        clearConsole: true, // in watch mode, clears console after every build
        processCssUrls: false,
        postCss: [
            //require('tailwindcss'),
            tailwindcss('./tailwind.config.js'),

            // to enable purgecss on production only
            ...process.env.NODE_ENV === 'production' ? [purgecss] : []

            // to enable on all environments, local and production
            //purgecss
        ],
    })

    .js("resources/js/app.js", "public/js")
    //.sass('resources/sass/libs/normalize.scss', 'public/css/libs')
    .sass("resources/sass/app.scss", "public/css")

    // libraries
    // tailwindcss: see https://tailwindcss.com/docs/installation#laravel-mix
    .sass("resources/sass/my_tailwind.scss", "public/css")

    // for the KeenBrain logo
    .copy("resources/img/brain.svg", "public/img/brain.svg")
    .copy("resources/img/heart.svg", "public/img/heart.svg")
    .sass("resources/sass/logo.scss", "public/css")

    // for the SVG animation
    .js("resources/js/libs/gs/*", "public/js/libs/gs");

//.disableNotifications() // if you want to disable OS notifications after every build
//.minify('public/css/app.css') // creates the min.css file but doesnt really minify?
