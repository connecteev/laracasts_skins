<?php 

	return [

		'levels' => ['beginner', 'intermediate', 'advanced'],
		'plan_durations' => ['day', 'month', 'year','forever'],

		'discussion' =>[
			'DISCUSSION_POINTS_POST_QUESTION'=>env('DISCUSSION_POINTS_POST_QUESTION',20),
			'DISCUSSION_POINTS_POST_REPLY'   =>env('DISCUSSION_POINTS_POST_REPLY',15),
			'DISCUSSION_POINTS_BEST_REPLY'   =>env('DISCUSSION_POINTS_BEST_REPLY', 50),
			'DISCUSSION_POINTS_NEW_REPLY_TO_QUESTION' => env('DISCUSSION_POINTS_POST_QUESTION', 5),
			'DISCUSSION_POINTS_REPLY_UPVOTED' => env('DISCUSSION_POINTS_REPLY_UPVOTED', 2),
			'DISCUSSION_POINTS_SELF_REPLY_UPVOTED' => env('DISCUSSION_POINTS_SELF_REPLY_UPVOTED', 1),
			'DISCUSSION_POINTS_QUESTION_FLAGGED_SPAM' => env('DISCUSSION_POINTS_QUESTION_FLAGGED_SPAM', -20),
			'DISCUSSION_POINTS_REPLY_FLAGGED_SPAM' => env('DISCUSSION_POINTS_REPLY_FLAGGED_SPAM',-15),
			'DISCUSSION_ON_USER_PROFILE' 		   => env('DISCUSSION_ON_USER_PROFILE', 10),
			'DISCUSSION_SPAM_THRESHOLD' 		   => env('DISCUSSION_SPAM_THRESHOLD', 2)
		],

		'plans' => [
			'monthly-15' => [
				'title'=>'Monthly',
				'amount'=>15,
				'details'=>"Still undecided? Ease in with a monthly plan that can be canceled in ten seconds.",
				'button'=>'Start Learning',
				'type'=>'normal',
				'duration'=>1,
				'period'=>'month',
				'number'=>1
			],
			'yearly-99' => [
				'title'=>'Yearly',
				'amount'=>99,
				'details'=>"This is your career we're talking about. Go all in, and save 45% off the monthly rate.",
				'button'=>'Start Learning',
				'type'=>'normal',
				'duration'=>1,
				'period'=>'year',
				'number'=>1
			],
			'forever'   => [
				'title'=>'Forever',
				'amount'=>350,
				'details'=>"We get it. Not everyone loves subscriptions. Pay once, and access Laracasts forever.",
				'button'=>'Start Learning',
				'type'=>'normal',
				'duration'=>0,
				'period'=>'forever',
				'number'=>1
			],
			'business-5' => [
				'title'=>'Small Biz',
				'amount'=>300,
				'details'=>"Provide up to <b>5 developers</b> on your team with a year's worth of access to Laracasts!",
				'button'=>'This looks good for me',
				'type'=>'business',
				'duration'=>1,
				'period'=>'year',
				'number'=>5
			],
			'business-10'=>[
				'title'=>'Over Medium',
				'amount'=>575,
				'details'=>"Provide up to <b>10 developers</b> on your team with a year's worth of access to Laracasts!",
				'button'=>'This looks good for me',
				'type'=>'business',
				'duration'=>1,
				'period'=>'year',
				'number'=>10
			],
			'business-25'=>[
				'title'=>'Big Business',
				'amount'=>1400,
				'details'=>"Provide up to <b>25 developers</b> on your team with a year's worth of access to Laracasts!",
				'button'=>'This looks good for me',
				'type'=>'business',
				'duration'=>1,
				'period'=>'year',
				'number'=>25
			],
			'business-50'=>[
				'title'=>'Park Place',
				'amount'=>2500,
				'details'=>"Provide up to <b>50 developers</b> on your team with a year's worth of access to Laracasts!",
				'button'=>'This looks good for me',
				'type'=>'business',
				'duration'=>1,
				'period'=>'year',
				'number'=>50
			]
		],
		'settings'=>[
			'owner_name' => env('OWNER_NAME', 'Kunal'),
			'email' 	 => env('EMAIL', 	  'admin@gmail.com')
		]

	];
